from ansible.module_utils.basic import AnsibleModule
from xlwt import Workbook
def main():
    module_args=dict(
        name=dict(type='str',required=True),
        age=dict(type='int')
    )
    module=AnsibleModule(
        argument_spec=module_args
    )
    
    name=module.params['name']
    age=module.params['age']

    wb=Workbook()
    sheet1=wb.add_sheet('sheet1')
    sheet1.write(0,0,name)
    if module.params['age']:
        sheet1.write(0,1,age)
    wb.save('/tmp/name.xls')

    module.exit_json(changed=True)

if __name__ == '__main__':
    main()
