# Đọc kỹ hướng dẫn sử dụng trước khi dùng
> Link Module: https://gitlab.com/lam1791998/home-training/-/blob/master/custom_module_ansible/fact_to_exel/fact_to_exel.py
### fact_to_exel - Save facts gathered to an exel file
- Module này được sử dụng để chọn ra các facts mà Ansible thu thập được ở các hosts sau nó lưu vào 1 file exel.
- Không hỗ trợ trên Window.
### Parameters 
| **Parameter** | **Type** | **Defaults/Choices**  | **Comment**  |
| ------------- | -------- | --------------------- | ------------ |
| fields        | string   | required              | Chỉ ra tên các cột chứa các facts |
| facts         | string   | required              | Chỉ ra những facts được lưu (thứ tự các fact để giống như parameter `fields` |
| path          | path     | Default: /tmp/fact_gathered.xlsx | Chỉ ra nơi lưu và tên file exel |
### Note
- **Quan trọng**: Để lưu file exel trên local phải thêm `delegate_to: localhost` vào playbook. Cách thêm xem trong Example.
- Lưu Module đúng nơi quy định:  
  - Bất kỳ đường dẫn nào được thêm vào biến môi trường `ANSIBLE_LIBRARY` 
  - `~/.ansible/plugins/modules/`
  - `/usr/share/ansible/plugins/modules/`
  - Nếu chỉ dùng cho 1 playbook nhất định thì ta lưu module trong dir `library` trong thư mục làm việc hiện tại ( thư mục chứa playbook ). Tương tự để dùng cho 1 role nhất định.  
- Sử dụng python 3 (nên là >= 3.6).
- Cài đặt lib `openpyxl-3.0.3`(trở lên) cho python.
  > Cài bản mới nhất bằng lệnh : `pip3 install openpyxl`  
### Example and result
```yaml
- name: Testing Module
  fact_to_exel:
    fields: ' Default_IPv4 Hostname Distribution Distribution_version BIOS_version OS_Kernel Total_RAM Total_Swap Cores  '
    facts: " {{ansible_default_ipv4.address}} {{ansible_hostname}} {{ansible_distribution}} {{ansible_distribution_version}} {{ansible_bios_version}} {{ansible_kernel}} {{ansible_memtotal_mb}} {{ansible_swaptotal_mb}} {{ansible_processor_cores}} "
    path: /home/dell/testansible/fact_gathered.xlsx
  delegate_to: localhost
```
![Screenshot_from_2020-04-14_16-19-44](/uploads/c5c4054037e916a5feb8839ebef7839d/Screenshot_from_2020-04-14_16-19-44.png)
### Authors
- Nguyễn Xuân Trường Lâm
- Nguyễn Văn Minh - Supporter =))) 
> Lưu ý: Khi run module mà có lỗi như là *The error was: zipfile.BadZipfile: File is not a zip file* hay lỗi gì tương tự thế, vui lòng xóa file exel (nếu đã có) và chạy lại module. NPH sẽ cố gắng khắc phục lỗi này trong tương lai =))) .
