from ansible.module_utils.basic import AnsibleModule
import openpyxl
import os

def main():

    module=AnsibleModule(
        argument_spec=dict(
            fields=dict(type='str',required=True),
            facts=dict(type='str',required=True),
            path=dict(type='str',default='/tmp/fact_gathered.xlsx'),
        ),
        supports_check_mode=True,
    )
    
    fields=list(str.split(module.params['fields']))
    facts=list(str.split(module.params['facts']))
    path=module.params['path']

    if os.path.exists(path):
        wb=openpyxl.load_workbook(path)
        sheet=wb.get_sheet_by_name('Sheet')
        j=2
        while sheet.cell(j,1).value!=None:
            j+=1
        sheet.cell(j,1).value=str(j-1)
        for i in range(len(facts)):
            sheet.cell(j,i+2).value=facts[i]
        wb.save(path)
    else:
        wb=openpyxl.Workbook()
        sheet=wb.get_sheet_by_name('Sheet')
        sheet.cell(1,1).value='STT'
        for i in range(len(fields)):
            sheet.cell(1,i+2).value=fields[i]
        sheet.cell(2,1).value='1'
        for i in range(len(facts)):
            sheet.cell(2,i+2).value=facts[i]
        wb.save(path)

    module.exit_json(changed=True)

if __name__ == '__main__':
    main()

