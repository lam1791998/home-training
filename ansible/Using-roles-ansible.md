@daikk115 @HaManhDong 
## Giới thiệu chút về roles
Trong khi viết 1 file playbook quá dài và cồng kềnh mà lại không thể tái sử dụng (tất nhiên là sửa chút thì vẫn tái được). Ansible cho phép tổ chức, chia nhỏ playbook ra thành nhiều file mà có thể tái sử dụng dễ dàng. Có 3 cách để làm điều này là: **includes** (added in version 2.0), **imports** (added in version 2.4), và cao cấp hơn là **roles**. 
Roles đã có mặt từ thủa sơ khai của Ansible và có thể upload an share via Ansible Galaxy. Roles cho phép không chỉ các tasks được gói lại vơi nhau mà còn thêm các variables, handlers thậm chí là các modules, plugins. 
## Role Directory Structure
Có 2 cách để tạo ra 1 roles: Có thể tạo thủ công theo đúng structure hoặc để thuận tiện, dùng `ansible-galaxy init <role-name>`. 
![image](/uploads/55fd4dd6acf83b08cc8a326f8c3b1799/image.png)

1 role phải chứa 1 trong các dir trên (trừ dir `test`) và mỗi dir phải có 1 file `main.yml`. Có thể có các file khác trong trong dir khác file `main.yml` nhưng đều phải được dẫn dắt từ file `main.yml` qua `import_tasks:` hay gì đấy.
- `tasks`: Chứa danh sách chính các tasks được thực thi bởi role.
- `handlers`: Chứa các handlers, có thể sử dụng trong role hoặc thậm trí ngoài role này.
- `defaults`: default variables for the role.
- `vars`: other variables for the role.
- `files`: Chứa các files có thể được deployed thông qua role.
- `templates`: Chứa các templates có thể được deployed thông qua role.
- `meta`: defines some metadata for this role, bao gồm tác giả, supported platforms và dependencies.
## Using roles
Sử dụng roles thông qua `roles:`. Ví dụ:
```yaml
---
- hosts: some-hosts
  roles:
    - role-1
    - role-2
```
Nếu dùng tên đơn giản như ở trên, Ansible sẽ search role theo thứ tự: 
- Directory `roles/` tại thư mục làm việc hiện tại (chứa playbook).
- `/etc/ansible/roles` (mặc định).
Hoặc có thể chơi kiểu path: 
```yaml
---
- hosts: some-hosts
  roles:
    - role: '/path/to/my/roles/role-1'
    - role: '/path/to/my/roles/role-2'
```
Ansible sẽ add tất cả những gì có trong file `main.yml` ở các dir của mỗi role vào trong play. Thứ tự thực thi của play sẽ như sau: 
- Any `pre_tasks` (gather_facts, ...) defined in the play.
- Any handlers triggered so far will be run.
- Môi role trong `roles:` sẽ được thực thi lần lượt.  Any role dependencies defined in the roles `meta/main.yml` sẽ được chạy trước, tùy thuộc vào tag filtering và conditionals.
- Any `tasks` defined in the play.
- Any handlers triggered so far will be run.
- Any `post_tasks` defined in the play.
- Any handlers triggered so far will be run.
Ngoài ra còn có 2 cách khác để sử dụng role (added in version 2.4):
- `import_role`: Là kiểu static giống như `roles:`, được xử lý trong quá trình phân tích cú pháp playbook.
- `include_role`: Là kiểu dynamic, nghĩa là khi đang chạy task mà gặp phải sẽ xử lý, thường hay đi kèm với conditional
Ví dụ:
```yaml
---
- hosts: some-hosts
  tasks:
    - import_role:
        name: role-1
    - include_role:
        name: role-2
      when: ansible_facts['os_family'] == 'Debian'
```
## Role Dependencies
Role Dependencies cho phép kéo các roles khác về trong khi đang sử dụng 1 role và các roles này sẽ được thực thi trước. Cũng có thể hiểu là 1 cách để chia nhỏ role giống như role để chia nhỏ playbook.
Ví dụ `/roles/role-3/meta/main.yml`:
```yaml
---
dependencies:
  - role: role-1
    vars:
      some_parameter: 3
  - role: role-2
    vars:
      other-parameter: 4
```
> Note: Role dependencies phải define kiểu classic như trên. Các dependencies cũng có thể có các dependencies, nếu 1 role xuất hiện nhiều lần trong dependency chaining nó sẽ chỉ được thực thi 1 lần nếu như các parameters defined trong dependency không khác nhau trừ khi thêm `allow_duplicates: true` vào file `/meta/main.yml` của các role con (ở ví dụ trên là role-1, role-2).
## Templating
Như đã nói ở trên thì dir `templates` của role chứa các templates - file viết bằng ngôn ngữ Jinja2 (.j2) được sử dụng bởi module **template** chủ yếu để quản lý file cấu hình trên các cụm 1 cách linh hoạt.
Ví dụ file `/templates/temp1.j2`: 
```jinja2
{{var1}}
{% for item in list1 %}
- {{item}}
{% endfor %}
{{var2}}
```
Lưu ý mức độ ưu tiên của variables (tăng dần):
1. Option `-u` trong command-line.
2. Role `defaults` (/defaults/main.yml).
3. Inventory group_vars.
4. Playbook group_vars.
5. Inventory host_vars.
6. Playbook host_vars.
7. Host facts / cached `set_facts`
8. Play `vars:`.
9. Play `vars_prompt:`.
10. Play `vars_files:`.
11. Role `vars` (/vars/main.yml).
12. Block `vars:` (chỉ tính cho các tasks trong block đó).
13. Task `vars:` (chỉ tính cho task đó).
14. `include_vars`:
15. Vars from module `set_facts` / registered vars .
16. `role` (and `include_role`) params.
17. `include` params.
18. Option `-e` trong command-line always win.
Ansilbe sẽ tìm giá trị của các biến `var1`, `var2` và `list1` trong `vars/main.yml` trước. Nếu trong đó không có thì sẽ tìm đến `defaults/main.yml`. Ví dụ file `vars/main.yml`: 
```yaml
---
# vars file for roletest
var1: 'Hello các anh!! Các bạn thực tập hôm nay bao gồm:'
```
File `defaults/main.yml`: 
```yaml
---
# defaults file for roletest
var1: 'Hello!!'
list1: ['Minh','Lâm','Toàn']
var2: 'End'
```
Và file `tasks/main.yml`:
```yaml
---
# tasks file for roletest
- template:
    src: temp1.j2
    dest: /home/dell/testansible/test.txt
```
Kết quả: 
![image](/uploads/423b87c9bac11013dd0ed1f4bc52f667/image.png)